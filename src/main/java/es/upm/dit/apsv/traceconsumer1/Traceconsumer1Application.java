package es.upm.dit.apsv.traceconsumer1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.function.Consumer;
/*con cambios*/
@SpringBootApplication
public class Traceconsumer1Application {

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer1Application.class, args);
	}




	public static final Logger log = LoggerFactory.getLogger(Traceconsumer2Application.class);

	@Autowired
	private  TraceRepository tr;
       @Autowired
	private  TransportationOrderRepository orders;

	@Bean("consumer")
	public Consumer<Trace> checkTrace() {
		return t -> {
			t.setTraceId(t.getTruck() + t.getLastSeen());
			tr.save(t);
                    TransportationOrder result = orders.findById(t.getTruck()).orElse(new TransportationOrder());
			if (result != null && result.getTruck()!= null
                        && ! result.getTruck().equals("")
                        && result.getSt() == 0) {
				result.setLastDate(t.getLastSeen());
				result.setLastLat(t.getLat());
				result.setLastLong(t.getLng());
				if (result.distanceToDestination() < 10)
					result.setSt(1);
				orders.save(result);
				log.info("Order updated: "+ result);
			}
		};
	}
}



